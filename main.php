<?php
    $connect = mysqli_connect('localhost','root', '', 'coursework');
    if(!$connect){
        die("Connection error!");
    }
    $channels = mysqli_query($connect, "SELECT * FROM `channel`");
    $hashtags = mysqli_query($connect, "SELECT * FROM `hashtag`");
?> 


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <header class="border-bottom">
        <div class="container navbar mt-2">
            <p><?=$_COOKIE['user']?></p>
            <a class="btn btn-primary" href="/exit.php">Выход</a>
        </div>
    </header>
    <main class="container" >
        <div class="justify-content-center mt-3" style="width: 500px;">
            <h1>Добавить сообщение</h1>
            <form action="/add-message.php" method="POST" class="flex-column">
                <div class="mt-4">
                    <label for="message" class="form-label">Тип сообщения</label>
                    <select name="message" id="message" class="form-select">
                    <option value="1" default>Публичное</option>
					<option value="2">Приватное</option>
                    </select>
                </div>
                <div class="mt-4">
                    <label for="channel" class="form-label">Канал для публикации</label>
                    <select name="channel" id="channel" name="channel" class="form-select">
                        <?php
                            $i = 1;
                            while($channel = mysqli_fetch_assoc($channels))
                            {
                                ?>
                                    <option value="<?php echo $i; ?>"><?php echo $channel['name'] ?></option>
                                    <?php
                                    $i++;
                            }
                        ?>
                    </select>
                </div>
                <div class="mt-4">
                    <label for="hashtag" class="form-label">Хэштэг</label>
                    <select name="hashtag" id="hashtag" name="hashtag" class="form-select">
                    <?php
                            $i = 1;
                            while($hashtag = mysqli_fetch_assoc($hashtags))
                            {
                                ?>
                                    <option value="<?php echo $i; ?>"> <?php echo $hashtag['name'] ?></option>
                                    <?php
                                    $i++;
                            }
                        ?>
                    </select>   
                </div>
                <div class="mt-4">
                    <label for="field" class="form-label">Область знаний</label>
                    <input type="search" id="field" name="field" class="form-control">
                </div>
                <div class="mt-4 mb-4">
                    <label for="text" class="form-label">Ваше сообщение</label>
                    <textarea name="text" id="text" rows="10" class="form-control"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Отправить</button>
            </form>
        </div>
        <section class="mt-5 container row" >
            <div class="row" >
                <h1>Сообщения</h1>
                <?php
                    $result = mysqli_query($connect, "SELECT * FROM `sms`");
                    $result = mysqli_fetch_all($result);
                    foreach($result as $item) {
                        $channel = $connect->query('SELECT name FROM `channel` WHERE `id` =' . $item[4])->fetch_assoc()['name'];
                        $field = $connect->query('SELECT name FROM `field` WHERE `id` =' . $item[5])->fetch_assoc()['name'];
                        $hashtag = $connect->query('SELECT name FROM `hashtag` WHERE `id` =' . $item[1])->fetch_assoc()['name'];
                        ?>
                        <div class="card card-body border-bottom col-3 m-3 p-3"> 
                            <h5 class="card-title">sms</h5>
                            <p class="card-text">
                                <?php 
                                    if ($item[2] == '2'){
                                        echo "Приватное";
                                    }
                                    else{
                                        echo "Публичное";
                                    }
                                ?> сообщение.
                                </p>
                            <p class="card-text">Пользователь: <?php echo $item['3'] ?></p>
                            <p class="card-text">Канал: <?php echo $channel ?></p>
                            <p class="card-text">Область знаний: <?php echo $field ?></p>
                            <p class="card-text">Хэштэг: <?php echo $hashtag ?></p>
                            <p class="card-text">Текст: <?php echo $item[6] ?></p>
                        </div>
                        
                <?php
                    }
                ?>
            </div>
        </section>
    </main>
</body>
</html>


    