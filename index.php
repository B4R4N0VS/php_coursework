<?php

// require_once 'connection.php';


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
   <?php
        if (!isset($_COOKIE['user'])):
   ?>
    <div class="container align-items-center">
        <div class="card card-body m-auto mb-4 mt-5" style="width: 400px;">
            <form action="/authorization.php" method="POST">
                <h2>Войти</h2>
                <div class="mb-3">
                    <label for="logUsername" class="form-label">Логин</label>
                    <input type="text" class="form-control" name="username" id="logUsername">
                </div>
                <div class="mb-3">
                    <label for="logPassword" class="form-label">Пароль</label>
                    <input type="password" class="form-control" name="password" id="logPassword">
                </div>
                <input type="text" name="type" value="log" style="display: none;">
                <button type="submit" class="btn btn-primary">Войти</button>
            </form>
        </div>
    </div>
    <?php
        else:
            require_once 'main.php';
    ?>
    <?php
        endif;
    ?>
    
</body>
</html>